class UI {
    constructor(gameLoop) {
        this.SELECTOR = '.over';
        this.HIDE_CLASS = 'hidden';
        this.gameLoop = gameLoop;

        this.screens = [];
        document.querySelectorAll(this.SELECTOR).forEach(scr => {
            this.screens.push(scr.id);
        });
    }

    show(screenId, pause = true) {
        if (!this.screens.includes(screenId)) {
            console.warn(`There's no screen named '${screenId}'.`);
            return;
        }

        document.querySelectorAll(this.SELECTOR).forEach(scr => {
            if (scr.id === screenId) {
                scr.classList.remove(this.HIDE_CLASS);
            } else {
                scr.classList.add(this.HIDE_CLASS);
            }
        });

        if (!pause) return;
        this.gameLoop.stop();
    }

    hideAll(unpause = true) {
        document.querySelectorAll(this.SELECTOR).forEach(scr => {
            scr.classList.add(this.HIDE_CLASS);
        });

        if (!unpause) return;
        this.gameLoop.start();
    }
}
