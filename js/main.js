/* ======== IMPORTS ======== */
let { Sprite, GameLoop, keyPressed } = kontra;

/* ======== SYSTEM VARIABLES ======== */
let canvas;
let loop;
let ui;

/* ======== GAME OBJECTS ======== */
let player;

/* ======== GAME LOOP ======== */
function init() {
    canvas = kontra.init('game').canvas;
    kontra.initKeys();

    player = Sprite({
        x: 390,
        y: 390,
        color: '#5a5',
        width: 20,
        height: 20
    });

    loop = GameLoop({
        update: update,
        render: render
    });

    ui = new UI(loop);

    loop.start();
}

function update() {
    let dx = 0, dy = 0;

    if (keyPressed('up') || keyPressed('w')) { dy -= 1; }
    if (keyPressed('down') || keyPressed('s')) { dy += 1; }
    if (keyPressed('left') || keyPressed('a')) { dx -= 1; }
    if (keyPressed('right') || keyPressed('d')) { dx += 1; }

    const diag = dx !== 0 && dy !== 0;
    dx *= diag ? DIAG_DX : SHIP_DX;
    dy *= diag ? DIAG_DX : SHIP_DX;

    player.x += (player.x + dx >= 0 && player.x + dx <= canvas.width - player.width) ? dx : 0;
    player.y += (player.y + dy >= 0 && player.y + dy <= canvas.height - player.height) ? dy : 0;
}

function render() {
    player.render();
}
